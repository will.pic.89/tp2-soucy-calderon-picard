<?php
/**
 * Plugin name: Taskmanager
 * Plugin URI: https:/fakewebsite.com/Taskmanager
 * Description: Essaie de pluggin pour un Taskmanager
 * Version: 0.1
 * Author: Soucy, Calderon, Picard
 * Author URI: takodevweb.com
 */


 defined('ABSPATH') or die ('You can\t access this file');

require __DIR__ . '/vendor/autoload.php';

use Taskmanager\Database\DataStore;
use Taskmanager\Database\Datatable;
use Taskmanager\CustomPost\TaskPostType;
use Taskmanager\Routes\RouteManager;
use Taskmanager\Fields\Metaboxes;

class Taskmanager
{
    /**
     * Taskmanager constructor.
     * @usage Add all needed classes initialization.
     */
    public function __construct()
    {
        add_action( 'init', [ $this, 'init' ] );
        register_activation_hook( __FILE__, [ $this, 'init_data' ] );
    }

    public function init()
    {
        new TaskPostType();
        new Metaboxes();
        new DataStore();
        new RouteManager();
    }

    public function init_data()
    {
        new Datatable();
    }

    public function deactivate()
    {
        flush_rewrite_rules();
    }

}

if ( class_exists('Taskmanager')) {
  $taskmanager =  new Taskmanager();
}

register_deactivation_hook( __FILE__, array($taskmanager, 'deactivate'));

