Nous avons créer un custom post pour le plugin: Taskmaganer, il se agit de la gestion de projet: créer et assigner des tâches aux participants. Nous avons ajouté des metaboxes personnalisées, et grâce à les metaboxes nous pouvons enregistrer des informations relatives à un tâche, ou à n’importe quel élément d’un Custom Post Type.

Après la création de notre custom post type:
Ici dossier ->CumstomPost ->TaskPostType.php
Nous avons eu un peu de mal à comprendre toutes ces options quand on débutais. On a donc parti du strict minimum puis d’ajouter des réglages et des labels au fur et à mesure des besoins.

On a crée le dossier->Fields ->Metaboxes.php
Un Metabox y a donc besoin d’un label et d’un champ texte, et on appelle donc l’objet $value puis via get_post_meta on récupère la valeur sauvegardée. Il faut donc appeler une nouvelle fonction lors de cet évènement.

Le add_action correspondant est save_post. Chose pratique : notre fonction prend directement en argument l’ID du contenu en cours d’enregistrement.

Nous utilisons l’objet $wpdb qui nous permet de communiquer avec notre table.

On a crée un dossier->Database:
->Datatable: Si la table (table_task) n’existe pas, on la créée avec toutes les colonnes décrites précédemment. On assigne à chaque colonne un nom, un type et la valeur par défaut qu’elle doit avoir.

->DataStore: il va sauvagarder nos donnes au BD.

On crée une dossier -> Routes, avec éventuellement des paramètres complémentaires, dans ce cas on crée deux routes.

->TaskRoutes: la possibilité de mettre un ID (sous forme de regex) : (?P<id>\d+) on attend là un nombre de 1 ou plusieurs caractères.
La route invoquée appellera en GET (on récupère seulement des infos. Utiliser PUT pour écrire et DELETE pour effacer) et appellera la fonction 'get_task_with_id'
la fonction 'get_task_with_id' vous permettra, exactement comme une WP Query. Dans l’URL fournie par la requête API, et on retourne les résultats. L’api s’occupe de la mise en page en JSON.

->RouteManager: ici on a fait le constructor pour le route, et on appelle a la route TaskRoutes dès ici.

L'archive -> TaskManager: Il va gérer tout les archives du pluging. 

