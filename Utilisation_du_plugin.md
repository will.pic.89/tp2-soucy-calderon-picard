Ce plugin est créé pour faciliter la répartion des tâches dans votre entreprise. Vous allez pouvoir créer des tâches, les modifier, les commenter ou même les supprimer à votre guise.

Création d'une tâche en détail:

    1. Activer le plugin dans les extensions installer.
    2. Aller dans l'onglet Gestionnaire de tâche.
    3. Faite un click sur le bouton ajouter.
    4. Ensuite il faut remplir les cases vide et mettre un titre.
    5. Pour fini il faut simplement faire un click sur le bouton publier.

Et voilà vous venez votre première tâche en utilisant notre plugin!!!