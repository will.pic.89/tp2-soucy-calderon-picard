<?php


namespace Taskmanager\Routes;


class TaskRoutes
{

    /**
     * TaskRoutes  constructor.
     */
    public function __construct()
    {
        $this->create_task_routes();
    }

    public function create_task_routes()
    {
        register_rest_route('taskmanager/v0', '/task-post/(?P<id>\d+)', array(
            'methods' => 'GET',
            'callback' => [$this,"get_task_with_id"],
        ));
        register_rest_route('taskmanager/v0', '/task-post/delete/(?P<id>\d+)', array(
            'methods' => 'DELETE',
            'callback' => [$this,"delete_task"],
        ));
        register_rest_route('taskmanager/v0', '/task-post', array(
            'methods' => 'GET',
            'callback' => [$this,"get_all_tasks"],
        ));
        register_rest_route('taskmanager/v0', '/task-post', array(
            'methods' => 'POST',
            'callback' => [$this,"create_task"],
        ));
    }

    public function get_all_tasks()
    {
        $args = array(
            'post_type'	 => 'task',
            'post_status'	 => 'publish',
            'posts_per_page' => -1
        );
        $query = new \WP_Query( $args );
        return rest_ensure_response($query->posts);
    }

    public function get_task_with_id($request)
    {
        $id = $request->get_param('id');
        $args = array(
            'post_type'	 => 'task',
            'p' => $id
        );
       
        
        $query = new \WP_Query( $args );
        return rest_ensure_response($query->posts);
    }

     public function create_task( \WP_REST_Request $request )
    {
        $args = [
            'post_title' => $_POST["task_field_title"],
            'post_id' => $post_id,
            'task_text' => $_POST["task_field_text"],
            'task_responsable' => $_POST["task_field_responsable"],
            'task_date' => $_POST["task_field_datepicker"],
            'task_status' => $_POST["task_field_status"]
        ];
        wp_insert_post( $args );
        return rest_ensure_response(json_encode($args));
    } 

    public function delete_task( \WP_REST_Request $request )
    {
        wp_delete_post( $request, $force_delete );
        $args = ['Message' => 'Le post a bien été surpimmé'];
        return rest_ensure_response(json_encode($args));
    } 
    
}