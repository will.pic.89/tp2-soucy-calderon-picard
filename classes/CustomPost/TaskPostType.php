<?php


namespace Taskmanager\CustomPost;


class TaskPostType
{
    public function __construct(  )
    {
        $this->create_task_post_type();
    }

    public function create_task_post_type(  )
    {
        $labels = array(
            'name'                => _x( 'Gestionnaire de Tâches', 'Post Type General Name', 'task'),
            'singular_name'       => _x( 'Tâches', 'Post Type Singular Name', 'task'),
            'menu_name'           => __( 'Gestionnaire de Tâches', 'task'),
            'all_items'           => __( 'Toutes les Tâches', 'task'),
            'view_item'           => __( 'Voir toutes les Tâches', 'task'),
            'add_new_item'        => __( 'Ajouter une nouvelle tâche', 'task'),
            'add_new'             => __( 'Ajouter', 'task'),
            'edit_item'           => __( 'Edit', 'task'),
            'update_item'         => __( 'Modifier', 'task'),
            'search_items'        => __( 'Rechercher', 'task'),
            'not_found'           => __( 'Vide', 'task'),
            'not_found_in_trash'  => __( 'Poubelle Vide', 'task'),
        );

        $args = array(
            'label'               => __( 'Tâches', 'task'),
            'description'         => __( 'Tâches', 'task'),
            'labels'              => $labels,
            'supports'            => array( 'title' ),
            'show_in_rest'        => true,
            'hierarchical'        => false,
            'public'              => true,
            'has_archive'         => true,
            'rewrite'			  => array( 'slug' => 'task-post'),

        );

        register_post_type( 'task', $args );
    }
}