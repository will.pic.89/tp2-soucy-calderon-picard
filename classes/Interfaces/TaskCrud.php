<?php


namespace Taskmanager\Interfaces;


interface TaskCrud
{
    public function create(  );
    public static function read(  );
    public function update(  );
    public function delete(  );
}