<?php


namespace Taskmanager\Database;


class DataStore
{
    /**
     * DataStore constructor.
     */
    public function __construct() {
        add_action('save_post_task', [$this, "save_task_data_to_database"]);
    }

    public function save_task_data_to_database( $post_id )
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "table_task";
        if (array_key_exists('task_field_text', $_POST)){
            $wpdb->insert($table_name, array(
                'post_id' => $post_id,
                'task_text' => $_POST["task_field_text"],
                'task_responsable' => $_POST["task_field_responsable"],
                'task_date' => $_POST["task_field_datepicker"],
                'task_status' => $_POST["task_field_status"]
            ));
        }
    }
}