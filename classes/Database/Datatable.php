<?php
namespace Taskmanager\Database;

use Taskmanager\Database\DataStore;

class Datatable
{
    /**
     * Initialization constructor.
     */
    private $current_db_version;
    public function __construct()
    {
        $this->database_init();   
    }

    public function database_init()
    {
        
            global $wpdb;
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            $sql = "CREATE TABLE IF NOT EXISTS `" . $wpdb->prefix . "table_task`(
                ID bigint(20) NOT NULL auto_increment,
                post_id varchar(255) default NULL,
                task_text longtext,
                task_responsable varchar(255),
                task_date DATE,
                task_status varchar(255),
                PRIMARY KEY  (`ID`)
            );";
            dbDelta( $sql );
        
    }

   
}

register_activation_hook( __FILE__, 'database_init' );