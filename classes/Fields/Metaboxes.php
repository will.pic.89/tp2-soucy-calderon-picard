<?php


namespace Taskmanager\Fields;

class Metaboxes
{
    public function __construct()
    {
        add_action( 'add_meta_boxes', [ $this, "add_task_textbox" ] );
        add_action( 'save_post_task', [ $this, "save_task_data" ] );
        
    }

    public function add_task_textbox()
    {
        add_meta_box(
            'task_box_id', 
            __( 'Description de la Tâche', 'task' ),  
            [ $this, 'task_metabox_html' ],  
            'task'                   
        );

        add_meta_box(
            'task_date_id',
            __( 'Date fin tâche', 'task' ),  
            [ $this, 'task_metabox_datepicker' ],  
            'task'                   
        );

        add_meta_box(
            'task_responsable_id', 
            __( 'Responsable', 'task' ),  
            [ $this, 'task_metabox_responsable' ],  
            'task'                  
        );

        add_meta_box(
            'task_status_id',
            __( 'Status', 'task' ),  
            [ $this, 'task_metabox_status' ],  
            'task'                   
        );
    }

    public function task_metabox_html( $post )
    {
        $value = get_post_meta( $post->ID, '_task_field_text', true );
        $roles = 'Admin'
        ?>

        

        <textarea name="task_field_text" id="task_field_text" class="widefat" cols="50" rows="5" <?php if($roles != 'Admin') {?> disabled <?php }?>><?php echo $value; ?></textarea>
        <?php
    }

    public function task_metabox_datepicker( $post )
    {
        $value = get_post_meta( $post->ID, '_task_field_datepicker', true );
        ?>

        <p> <input type="date" id="task_field_datepicker" name="task_field_datepicker" value='<?php echo $value; ?>'></p>
        <?php
    }

    public function task_metabox_responsable( $post )
    {
        $value = get_post_meta( $post->ID, '_task_field_responsable', true );
        ?>

        <p>Nom: <input type="text" name="task_field_responsable" id="task_field_responsable" value='<?php echo $value; ?>' ></p>
        <?php
    }

    public function task_metabox_status( $post )
    {
        $value = get_post_meta( $post->ID, '_task_field_status', true );
        ?>

        <p>Nom: <select name="task_field_status" id="task_field_status">
            <option value="faire">à faire</option>
            <option value="cour">en cour</option>
            <option value="verifier">à vérifier</option>
            <option value="terminer">terminer</option>
        </select> </p>
        <?php
    }

    public function save_task_data( $post_id )
    {
        if( array_key_exists( 'task_field_text', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_task_field_text',
                wp_filter_nohtml_kses( $_POST[ 'task_field_text' ] )
            );
        }
        if( array_key_exists( 'task_field_datepicker', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_task_field_datepicker',
                wp_filter_nohtml_kses( $_POST[ 'task_field_datepicker' ] )
            );
        }
        if( array_key_exists( 'task_field_responsable', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_task_field_responsable',
                wp_filter_nohtml_kses( $_POST[ 'task_field_responsable' ] )
            );
        }
        if( array_key_exists( 'task_field_status', $_POST ) ) {
            update_post_meta(
                $post_id,
                '_task_field_status',
                wp_filter_nohtml_kses( $_POST[ 'task_field_status' ] )
            );
        }
    }

}